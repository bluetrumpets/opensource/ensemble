import * as React from 'react';
import { StandardProps } from '@material-ui/core';

export interface NavbarScrollButtonProps
  extends StandardProps<React.HTMLAttributes<HTMLDivElement>, NavbarScrollButtonClassKey> {
  /**
   * The content of the component.
   */
  children?: React.ReactNode;
  /**
   * Which direction should the button indicate?
   */
  direction: 'left' | 'right';
  /**
   * If `true`, the element will be disabled.
   */
  disabled?: boolean;
  /**
   * The tabs orientation (layout flow direction).
   */
  orientation: 'horizontal' | 'vertical';
}

export type NavbarScrollButtonClassKey = 'root' | 'vertical' | 'disabled';
/**
 *
 * Demos:
 *
 * - [Tabs](https://material-ui.com/components/tabs/)
 *
 * API:
 *
 * - [TabScrollButton API](https://material-ui.com/api/tab-scroll-button/)
 */
export default function NavbarScrollButton(props: NavbarScrollButtonProps): JSX.Element;
