import React from 'react';
import Navbar from './../Navbar';
import { render, screen, fireEvent, cleanup, getByTestId, getByDisplayValue } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import NavbarChild from '../../NavbarChild/NavbarChild';

describe("Navbar:", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        render(<Navbar />, div);
    });

    describe("Center alignment:", () => {
        describe("isCentered", () => {
            it("Navbar children are centered", () => {
                const div = document.createElement("div");
                const { getByTestId } = render(<Navbar isCentered={true}></Navbar>, div)
                expect(getByTestId("children-container")).toHaveStyle(`justify-content: center`);
            });
        });
        describe("isNotCentered", () => {
            it("Navbar children are not centered", () => {
                const div = document.createElement("div");
                const { getByTestId } = render(<Navbar isCentered={false}></Navbar>, div);
                expect(getByTestId("children-container")).not.toHaveStyle(`justify-content: center`);
            });
        });
    });

    describe("Orientation:", () => {
        describe("Vertical:", () => {
            it("Renders vertically", () => {
                const div = document.createElement("div");
                const { getByTestId } = render(<Navbar orientation="vertical"></Navbar>, div);
                expect(getByTestId("children-container")).toHaveStyle('flex-direction: column');
            });
        });
        describe("Horizontal:",() => {
            it("Renders horizontally", () => {
                const div = document.createElement("div");
                const { getByTestId } = render(<Navbar orientation="horizontal"></Navbar>,div);
                expect(getByTestId("children-container")).not.toHaveStyle('flex-direction: column');
            });
        });
    });

    describe("Scroll Buttons:", () => {
        it("Renders Scroll Buttons when on:", () => {
            const div = document.createElement("div");
            const {getAllByTestId} = render(<Navbar scrollButtons="on"></Navbar>,div);
            const arrayOfScrollButtons = getAllByTestId("scroll-button");
            for(const i in arrayOfScrollButtons){
                expect(arrayOfScrollButtons[i]).toBeInTheDocument();
            }
        });
        it("Renders Scroll Buttons when desktop:", () => {
            const div = document.createElement("div");
            const {getAllByTestId} = render(<Navbar scrollButtons="desktop"></Navbar>,div);
            const arrayOfScrollButtons = getAllByTestId("scroll-button");
            for(const i in arrayOfScrollButtons){
                expect(arrayOfScrollButtons[i]).toBeInTheDocument();
            }
        });
        //i don't know why the buttons don't render on auto but they don't
        //it's bad to leave it like this
        //need further investigation
        it("Does not render Scroll Buttons when auto:", () => {
            const div = document.createElement("div");
            const {queryByTestId} = render(<Navbar scrollButtons="auto"></Navbar>,div)
            expect(queryByTestId("scroll-button")).not.toBeInTheDocument();
        });
        it("Does not render Scroll Buttons when off:", () => {
            const div = document.createElement("div");
            const {queryByTestId} = render(<Navbar scrollButtons="off"></Navbar>,div)
            expect(queryByTestId("scroll-button")).not.toBeInTheDocument();
        });
    });

    describe("Indicator color:", () => {
        it("is primary:", () => {
            const div = document.createElement("div");
            const {getByTestId} = render(<Navbar indicatorColor='primary'></Navbar>,div);
            expect(getByTestId("indicator")).toHaveStyle('background-color: #3f51b5');
        });
        it("is secondary:", () => {
            const div = document.createElement("div");
            const {getByTestId} = render(<Navbar indicatorColor='secondary'></Navbar>,div);
            expect(getByTestId("indicator")).toHaveStyle('background-color: #f50057');
        });
    });
    describe("selected button text color:", () => {
        it("is primary:", () => {
            const div = document.createElement("div");
            const {getByTestId,getByText} = render(
                <Navbar
                    textColor = 'primary'
                >
                    <NavbarChild label="One" targetRef={React.createRef()}/>
                    <NavbarChild label="Two" targetRef={React.createRef()}/>
                </Navbar>
            ,div);

            userEvent.click(getByText('Two'), {skipHover:true});
            expect(getByTestId('button-child-true')).toHaveStyle('color: #3f51b5');
        });
        it("is secondary:", () => {
            const div = document.createElement("div");
            const {getByTestId,getByText} = render(
                <Navbar
                    textColor = 'secondary'
                >
                    <NavbarChild label="One" targetRef={React.createRef()}/>
                    <NavbarChild label="Two" targetRef={React.createRef()}/>
                </Navbar>
            ,div);

            userEvent.click(getByText('Two'), {skipHover:true});
            expect(getByTestId('button-child-true')).toHaveStyle('color: #f50057');
        });
    });
});
