import { ClickAwayListener } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import React from "react";
import useResizeObserver from "use-resize-observer";
import Container from "@material-ui/core/Container";
import Path from "./Path";
import ChildButton from "./ChildButton";

const DEFAULT_MASS = 1;
const DEFAULT_TENSTION = 500;
const DEFAULT_FRICTION = 17;
const DEFAULT_ROTATION = 0;
const DEFAULT_RADIUS = 100;

const useStyles = makeStyles({
  root: {
    position: "relative"
  },

  mainContent: {
    position: "absolute",
    zIndex: 1
  }
});

export default function Main(props) {
  const {
    centerContent,
    children,
    open,
    mass = DEFAULT_MASS,
    tension = DEFAULT_TENSTION,
    friction = DEFAULT_FRICTION,
    pathRadius = DEFAULT_RADIUS,
    hideBounce,
    rotation = DEFAULT_ROTATION,
    orientation
  } = props;

  const classes = useStyles(props);

  const { ref, height = 0, width = 0 } = useResizeObserver();

  const [_open, setOpen] = React.useState(!!open);

  React.useEffect(() => {
    setOpen(!!open);
  }, [open]);

  const onMain = () => {
    setOpen(true);
  };

  const onClickAway = (e) => {
    setOpen(false);
  };

  let childButtons = (React.ReactElement = []);
  let childCount = React.Children.count(children);

  React.Children.forEach(children, (c, i) => {
    childButtons[i] = (
      <ChildButton
        key={i}
        index={i}
        open={_open}
        childCount={childCount}
        mainHeight={height}
        mainWidth={width}
        mass={mass}
        friction={friction}
        pathRadius={pathRadius}
        rotation={rotation}
        orientation={orientation}
      >
        {c}
      </ChildButton>
    );
  });

  return (
    <ClickAwayListener onClickAway={onClickAway}>
      <div className={classes.root}>
        {hideBounce && (
          <Path
            open={_open}
            mainHeight={height}
            mainWidth={width}
            mass={mass}
            friction={friction}
            tension={tension}
            pathRadius={pathRadius}
          />
        )}
        <>{childButtons}</>
        <div className={classes.mainContent} onClick={onMain}>
          <Container open={_open}>
            <div ref={ref}>{centerContent}</div>
          </Container>
        </div>
      </div>
    </ClickAwayListener>
  );
}
