import { makeStyles } from "@material-ui/core";
import React from "react";
import { animated, useSpring } from "react-spring";

function getInitialPosition(mainWidth, mainHeight) {
  return {
    width: 0,
    height: 0,
    top: mainHeight / 2,
    left: mainWidth / 2,
    opacity: 0
  };
}

function getFinalPosition(mainWidth, mainHeight, pathRadius) {
  return {
    width: pathRadius * 2,
    height: pathRadius * 2,
    top: 0 - pathRadius + mainHeight / 2,
    left: 0 - pathRadius + mainWidth / 2,
    opacity: 1
  };
}

export default function Path(props) {
  const {
    pathRadius,
    mainWidth,
    mainHeight,
    open,
    mass,
    tension,
    friction
  } = props;

  const position = useSpring({
    reverse: !open,
    from: getInitialPosition(mainWidth, mainHeight),
    to: getFinalPosition(mainWidth, mainHeight, pathRadius),
    config: { mass, tension, friction }
  });

  return <animated.div style={position} />;
}
