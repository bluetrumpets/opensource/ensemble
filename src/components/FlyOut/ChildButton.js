import { makeStyles } from "@material-ui/core";
import React from "react";
import { animated, useSpring } from "react-spring";
import useResizeObserver from "use-resize-observer";
import Container from "@material-ui/core/Container";

function getFinalPostion(
  index,
  childCount,
  width,
  height,
  mainWidth,
  mainHeight,
  pathRadius,
  rotation,
  orientation
) {
  let { deltaX, deltaY } = getFinalDeltaPosition(
    index,
    childCount,
    width,
    height,
    pathRadius,
    rotation,
    orientation
  );

  const transform = { transform: "rotate(" + 0 + "deg)" };

  return {
    top: mainHeight / 2 + deltaX,
    left: mainWidth / 2 - deltaY,
    opacity: 1,
    ...transform
  };
}

function getInitialPosition(width, height, mainWidth, mainHeight) {
  return {
    top: mainHeight / 2 - height / 2,
    left: mainWidth / 2 - width / 2,
    opacity: 0
  };
}

function getFinalDeltaPosition(
  index,
  childCount,
  width,
  height,
  pathRadius,
  rotation,
  orientation
) {
  let SEPARATION_ANGLE;
  switch (orientation) {
    case "Circular":
      SEPARATION_ANGLE = 360 / childCount;
      break;

    case "Radial":
      SEPARATION_ANGLE = 180 / childCount;
      break;

    default:
      SEPARATION_ANGLE = 120 / childCount;
  }

  const FAN_ANGLE = (childCount - 1) * SEPARATION_ANGLE;
  const BASE_ANGLE = (180 - FAN_ANGLE) / 2 + 180 + rotation;

  const TARGET_ANGLE = BASE_ANGLE + index * SEPARATION_ANGLE;

  return {
    deltaX: pathRadius * Math.cos(toRadians(TARGET_ANGLE)) - height / 2,
    deltaY: pathRadius * Math.sin(toRadians(TARGET_ANGLE)) + width / 2,
    angle: TARGET_ANGLE
  };
}

const toRadians = (degrees) => {
  return degrees * (Math.PI / 180);
};

export default function ChildButton(props) {
  const {
    children,
    index,
    childCount,
    open,
    mainWidth,
    mainHeight,
    tension,
    friction,
    mass,
    pathRadius,
    rotation,
    orientation
  } = props;

  const useStyles = makeStyles({
    root: (props) => ({
      position: "absolute",
      zIndex: open ? 2 : 0
    })
  });

  const classes = useStyles(props);

  const { ref, height = 0, width = 0 } = useResizeObserver();

  const position = useSpring({
    reverse: !open,
    from: getInitialPosition(width, height, mainWidth, mainHeight),
    to: getFinalPostion(
      index,
      childCount,
      width,
      height,
      mainWidth,
      mainHeight,
      pathRadius,
      rotation,
      orientation
    ),
    config: { mass, tension, friction }
  });

  return (
    <animated.div className={classes.root} style={position}>
      <Container>
        <div ref={ref}>{children}</div>
      </Container>
    </animated.div>
  );
}
