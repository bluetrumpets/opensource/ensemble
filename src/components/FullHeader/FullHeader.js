import React from "react";
import PropTypes from 'prop-types';
import { createStyles, makeStyles } from "@material-ui/core";


let useStyles = makeStyles(theme=>createStyles({
  header : props=>({
    position : 'relative',  
    backgroundImage: `url(${props.bgImg})`,
    backgroundColor: props.bgColor,
    backgroundSize: "cover",
    height: props.height,
    width: "100%",    
  }),
  wrapper : props=>({
    position : 'absolute',
    ...props.wrapper
  })
}))



export function FullHeader(props) {
  const { title, subtitle, bgColor, position, height,
          bgImg, offset } = props;
  let wrapper = {}
  if(position === 'center') {
    wrapper = {
      left: "50%",
      marginLeft: "-50px",
      top: "50%",
      marginTop: "-50px"
    }
  } else {
    let [y,x] = position.split("-")
    wrapper = {
      [x] : offset.x,
      [y] : offset.y
    }
  }
  
  const classes = useStyles({
    bgColor,
    height,
    bgImg,
    wrapper
  })

  const component = (
      <section className={classes.header}>
        <section className={classes.wrapper}>
            {title}
            {subtitle}
        </section>
      </section>
  );

  return component;
}

FullHeader.propTypes = {
  title: PropTypes.element.isRequired, 
  subtitle: PropTypes.element.isRequired, 
  bgColor: PropTypes.string, 
  bgImg: PropTypes.string,
  height: PropTypes.oneOf([
      PropTypes.number, 
      PropTypes.string]),
  position : PropTypes.oneOf(['center', 'top-left', 'top-right','bottom-left', 'bottom-right']),
  offset: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
      }),
}

FullHeader.defaultProps = {
  bgColor: "#ccc",
  height: "100vh",
  bgImg: "",
  position : 'center',
  offset : {x : 0, y : 0}
};

