import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Switch from "@material-ui/core/Switch";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  Box: {
    height: 350,
    width: 360,
    [theme.breakpoints.down("sm")]: {
      width: "30%"
    }
  },
  Content: {
    position: "relative",
    top: -120,
    left: -190,
    width: "200%"
  },
  Reject: {
    position: "relative",
    top: 100,
    right: -540,
    [theme.breakpoints.down("sm")]: {
      top: 100,
      right: -50
    }
  },
  Accept: {
    position: "relative",
    top: 100,
    right: -550,
    [theme.breakpoints.down("sm")]: {
      top: 100,
      right: -50
    }
  },
  accordion: {
    width: "200%",
    fullwidth: "true",
    top: 10,
    left: -170
  },
  necessary: {
    position: "relative",
    top: -5,
    left: 460,
    [theme.breakpoints.down("xs")]: {
      top: -5,
      left: 10
    },
    [theme.breakpoints.down("sm")]: {
      top: -5,
      left: 10
    }
  },
  functionalities: {
    position: "relative",
    top: -5,
    left: 340,
    [theme.breakpoints.down("xs")]: {
      top: -5,
      left: 10
    },
    [theme.breakpoints.down("sm")]: {
      top: -5,
      left: 10
    }
  },
  enhancement: {
    position: "relative",
    top: -5,
    left: 405,
    [theme.breakpoints.down("xs")]: {
      top: -5,
      left: 10
    },
    [theme.breakpoints.down("sm")]: {
      top: -5,
      left: 10
    }
  },
  measurement: {
    position: "relative",
    top: -5,
    left: 485,
    [theme.breakpoints.down("xs")]: {
      top: -5,
      left: 10
    },
    [theme.breakpoints.down("sm")]: {
      top: -5,
      left: 10
    }
  },
  advertising: {
    position: "relative",
    top: -5,
    left: 420,
    [theme.breakpoints.down("xs")]: {
      top: -5,
      left: 10
    },
    [theme.breakpoints.down("sm")]: {
      top: -5,
      left: 10
    }
  },
  Continue: {
    position: "relative",
    top: 50,
    right: -370,
    [theme.breakpoints.down("sm")]: {
      top: 60,
      right: -50
    }
  }
}));

export default function CookieDialog() {
  const classes = useStyles();

  const [state, setState] = React.useState({
    necessary: true,
    functionalities: false,
    enhancement: false,
    measurement: false,
    advertising: false
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const [popOpen, setPopOpen] = React.useState(true);

  const handleClick = () => {
    setPopOpen(true);
  };

  const handleClose = () => {
    setPopOpen(false);
  };

  return (
    <div>
      <Popover
        open={popOpen}
        onClose={handleClose}
        disableEscapeKeyDown="true"
        disableBackdropClick="true"
        anchorOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "center"
        }}
      >
        {/*Content of the Policy*/}

        <Box p={28} className={classes.Box}>
          <Typography className={classes.Content} align="left">
            <h4>Notice</h4>
            As I now understand, the paragraph={true} attribute in Typography
            just adds a bottom-margin to the whole text. I.E. my one block of
            text is a paragraph. If I want another paragraph I would have to add
            another Typography
            <Grid item>
              <Button className={classes.Reject} onClick={handleClose}>
                Reject
              </Button>
              <Button className={classes.Accept} onClick={handleClose}>
                Accept
              </Button>
            </Grid>
          </Typography>

          {/*Customization options of Cookies*/}

          {/*Strictly Necessary*/}
          <Accordion className={classes.accordion}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              Strictly Necessary
              <Switch
                className={classes.necessary}
                disabled
                checked={state.necessary}
                onChange={handleChange}
                name="necessary"
                color="primary"
              />
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
                eget.
              </Typography>
            </AccordionDetails>
          </Accordion>

          {/*Basic interactions & functionalities*/}
          <Accordion className={classes.accordion}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              Basic interactions & functionalities
              <Switch
                className={classes.functionalities}
                checked={state.functionalities}
                onChange={handleChange}
                name="functionalities"
              />
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
                eget.
              </Typography>
            </AccordionDetails>
          </Accordion>

          {/*Experience enhancement*/}
          <Accordion className={classes.accordion}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              Experience enhancement
              <Switch
                className={classes.enhancement}
                switchBase="primary"
                checked={state.enhancement}
                onChange={handleChange}
                name="enhancement"
              />
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
                eget.
              </Typography>
            </AccordionDetails>
          </Accordion>

          {/*Measurement*/}
          <Accordion className={classes.accordion}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              Measurement
              <Switch
                className={classes.measurement}
                switchBase="primary"
                checked={state.measurement}
                onChange={handleChange}
                name="measurement"
              />
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
                eget.
              </Typography>
            </AccordionDetails>
          </Accordion>

          {/*Targeting & Advertising*/}
          <Accordion className={classes.accordion}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              Targeting & Advertising
              <Switch
                className={classes.advertising}
                switchBase="primary"
                checked={state.advertising}
                onChange={handleChange}
                name="advertising"
              />
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
                eget.
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Button className={classes.Continue}>Save & Continue</Button>
        </Box>
      </Popover>
    </div>
  );
}
