# Why Ensemble

We expect to contribute components that are either

1. unavailable
2. difficult to find / use
3. do not play well with [Material UI library](https://material-ui.com/)

# What stage of maturity are we at

This is not a battle tested library. We will update the README with components we think are ready for consumption. It is also a reason we have not yet invested in guides. We will probably write a developer guide before we write a user guide (and maintain it well!)

## Why did we release so early?

We want feedback. We want contributors. We want to build an ecosystem.

Writing code in silos has never been a good thing.

# Why Material UI?

It rocks! It has wide adoption, and seems to have a very sensible set of default behaviours and conventions.

We might have hard dependency on Material UI initially, but we intend to reduce it to the styling library over a period of time so that the consumers have a choice.

# We are opinionated

We are and intend to stay opinionated for some time. Once we have the ability to introduce abstractions and reduce dependencies, we will.

Doing that from day one will lead us to reinvent the wheel more often than is sensible.

# What to expect in near future?

- Lotsa components
- Monorepo architecture
