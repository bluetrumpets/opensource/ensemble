import babel from 'rollup-plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import external from 'rollup-plugin-peer-deps-external';
import resolve from '@rollup/plugin-node-resolve';
import postcss from 'rollup-plugin-postcss';
import image from '@rollup/plugin-image'
import visualizer from 'rollup-plugin-visualizer';
import json from '@rollup/plugin-json';
import multiInput from 'rollup-plugin-multi-input';


const extensions = ['.js', '.jsx', '.ts', '.tsx'];

export default {
    input: ['./src/index.js', '/src/App.js'],
    output : [
    {
      dir: 'dist',
      format: 'cjs'
    }
    ],
    plugins: [
        json(),
        external(),
        multiInput({
          exports: true,
          relative: './src' 
        }),
        postcss(),
        babel({
          exclude: 'node_modules/**',
          extensions
        }),
        resolve({
          extensions,
          mainfields:['./src/examples/SectionOnPage']
        }),
        commonjs(),
        image(),
        visualizer()
      ]
    };
